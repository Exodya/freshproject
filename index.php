<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <link rel="stylesheet" href="css/estilo.css">
    <link rel="icon" href="img/car/logo1.svg.png">
    <link href="https://fonts.googleapis.com/css2?family=Liu+Jian+Mao+Cao&display=swap" rel="stylesheet">
    <title>Tesla</title>
</head>
<body>
    <header class="site-header inicio">
        <div class="contenedor contenido-header">
        <div class="triangulo"></div><h1>ESL</h1> <div class="triangulo2"></div>
        </div>
            <div class="nav">
                <nav class="navegacion">
                   <ul>
                       <a href="/models.php"><li>MODELS</li></a>
                       <a href="/store.php"><li>STORE</li></a>
                       <a href="/contact.php"><li>CONTACT US</li></a>
                       <a href="/know.php"><li>KNOW US</li></a>
                   </ul>
                </nav>
            </div>
    </header>

    <section class=" seccion1 fondo">
       <div class="car" style="background-image:  url('img/car/backgroud.jpg');">
        <marquee> 
            <img src="img/car/teslaportada.jpg" alt="">
        </marquee>
        <marquee> 
            <img src="img/car/tesladeportada2.jpg" alt="">
        </marquee>
        <marquee> 
            <img src="img/car/maxresdefault.jpg" alt="">
        </marquee>
       </div>
    </section>

    <section>
        <div class="contenedor contenedor2 ">
            <div class="triangulo3"></div><h1>ESL</h1><div class="triangulo4"></div>
            </div>
        <br>
       <div class="information2">
           <div>
                <div class="beneficios">
                    <h4>Beneficios</h4>
                    <div class="lista">   
                    <ul>
                        <div class="triangulo5"></div><br>
                        <div class="triangulo5"></div><br>
                        <div class="triangulo5"></div><br> 
                        <div class="triangulo5"></div><br> 
                     </ul>
                    <ul>
                       <li>Impuesto de matriculación cero</li> 
                       <li>Aparcamiento Gratuito</li> 
                       <li>Carga gratuita en ciudades y aparcamientos de uso público</li> 
                        <li>Facilidades para cargadores en aparcamientos comunitarios</li> 
                    </ul>
                    <div class="auto1">
                        <div class="triangulo6"></div>
                        <div class="triangulo7"></div>
                        <div class="trapecio"></div>
                    <div class="bor1"></div>
                    <div class="bor2"></div>
                </div>
                </div></div>               
           </div>
           
           <div>

           </div>
       </div>
    </section>

    <section class="imagen-contacto fondo">
        <div class="tesla-information">
        <div class="information">
         <a href="#car-img"><h2>TESLA CASRS</h2></a>
          <p>Tesla, Inc. es una empresa estadounidense ubicada en Silicon</p>
          <p>Valley, California, y liderada por Elon Musk que diseña, fabrica y</p>
          <p>vende coches eléctricos, componentes para la propulsión de</p>
          <p>vehículos eléctricos y baterías domésticas a gran escala.​ Tiene su</p>
          <p>sede en Palo Alto, California.</p>
        </div>

        <div class="img">
            <img src="img/car/tesla-x.gif" alt="" id="car-img">
        </div>
    </div>
    </section>
    <footer class="site-footer seccion">     
            <div class="nav">
                <nav class="navegacion">
                   <ul>
                       <a href="#"><li>MODELS</li></a>
                       <a href="#"><li>STORE</li></a>
                       <a href="#"><li>CONTACT US</li></a>
                       <a href="#"><li>KNOW US</li></a>
                   </ul>
                </nav>
            </div>    
            <p class="copyrigth">Todos los derechos reservados 2020&copy;</p>
    </footer>
</body>
</html>